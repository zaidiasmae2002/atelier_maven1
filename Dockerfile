FROM openjdk:8-jre-alpine
COPY target/*.jar app.jar

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
